# clojure-tests

Various Clojure Tests

## echo-test

To run the server:

    lein run -m clojure-tests.echo-server -s

To run the client:

    lein run -m clojure-tests.echo-server -c

Both will continue to run until you enter an empty string into the
client.


## ZeroMQ

To use ZeroMQ from Clojure

- Install the libraries.  On the Mac: `brew install --with-libpgm --with-libsodium zeromq`
- Build/Instal ZeroMQ's Java bindings:

	git clone git@github.com:zeromq/jzmq.git
	cd jzmq
	./autogen.sh
	./configure --prefix=/usr/local/Cellar/jzmq/3.1.0
	make
	make install
- See the `project.clj` for additional config

## Multithreaded Code

- The 0MQ *context* object is thread-safe.
- Create a 0MQ *context* object for the process and pass it to all
  threads that need to communicate via *inproc* sockets.
- Do not share 0MQ *sockets* between threads.  They are not
  thread-safe.
- Do not use or close sockets except in the same thread that created
them.


## License

Copyright © 2015 FIXME

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
