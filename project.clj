(defproject clojure-tests "0.1.0-SNAPSHOT"
  :description "Miscellaneous Clojure Tests"
  :url "http://www.zimbra.com"
  :license {:name "Apache 2.0"
            :url "http://directory.fsf.org/wiki/License:Apache2.0"}
  :repositories [["sonatype" {:url "https://oss.sonatype.org/content/repositories/snapshots"
                              :update :always}]]
  :jvm-opts ["-Djava.library.path=/usr/lib:/usr/local/lib"]
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.zeromq/cljzmq "0.1.5-SNAPSHOT"]
                 [ring/ring-core "1.3.2"]
                 [ring/ring-jetty-adapter "1.3.2"]
                 [compojure "1.3.2"]
                 [ring/ring-json "0.3.1"]
                 [clj-http "1.0.1"]
                 [org.clojure/core.async "0.1.346.0-17112a-alpha"]
                 ;; the following two entries are to support sqlite
                 [org.clojure/java.jdbc "0.3.6"]
                 [org.xerial/sqlite-jdbc "3.8.7"]
                 [org.clojure/tools.cli "0.3.1"]])
