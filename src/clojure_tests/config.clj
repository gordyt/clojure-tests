(ns clojure-tests.config
  (:require [clojure.tools.reader.edn :as edn]))


(defn deep-merge
  "Deep merge two maps"
  [& values]
  (if (every? map? values)
    (apply merge-with deep-merge values)
    (last values)))

(defn load-config
  "Load config from the specified FILES"
  [& files]
  (reduce deep-merge (map (comp edn/read-string slurp)
                          files)))
          
(defn load-config-edns
  "Load config from the specified EDN strings"
  [& edns]
  (reduce deep-merge (map edn/read-string
                          edns)))

(defn load-config-maps
  "Load config from the specified EDN strings"
  [& maps]
  (reduce deep-merge maps))
                     



