(ns clojure-tests.echo-server
  (:require [clojure.string :as string]
            [zeromq.zmq :as zmq]
            [clojure.tools.cli :refer [parse-opts]])
  (:gen-class))

(def port 5555)

(def cli-options
  [["-h" "--help" "Print help"]
   ["-s" "--server" "Run the echo server"]
   ["-c" "--client" "Run the echo client"]])

(defn- usage [summary]
  (string/join
   \newline
   ["echo-server - echo client/server test program."
    ""
    "Usage: program-name [options]"
    summary]))


(defn client []
  (printf "Connecting to echo server on port %d\n" port)
  (flush)
  (let [context (zmq/context 1)
        get-data (fn [] (print "enter string or hit <RET> to exit: ") (flush) (read-line))]
    (with-open [socket (doto (zmq/socket context :req)
                         (zmq/connect (format "tcp://127.0.0.1:%d" port)))]
      (loop [data (get-data)]
        (when (> (.length data) 0)
          ;; send it
          (zmq/send-str socket data)
          (let [echo (zmq/receive-str socket)]
            (printf "received from server: '%s'\n" echo)
            (flush))
          (recur (get-data))))
      (println "exiting client...")
      (zmq/send-str socket ""))))
      


(defn server []
  (printf "Starting echo server on port %d\b\n" port)
  (flush)
  (let [context (zmq/context 1)]
    (with-open [socket (doto (zmq/socket context :rep)
                         (zmq/bind (format "tcp://*:%d" port)))]
      (while (not (.. Thread currentThread isInterrupted))
        (let [reply (zmq/receive-str socket)]
          (printf "received from client: '%s'\n" reply)
          (flush)
          (if (= (.length reply) 0)
            (do
              (println "exiting server...")
              (.. Thread currentThread interrupt))
            (zmq/send-str socket reply)))))))

(defn exit [status msg]
  (println msg)
  (System/exit status))

(defn -main [& args]
  (let [{:keys [options arguments errors summary]} (parse-opts args cli-options)]
    (cond
      (:help options) (exit 0 (usage summary))
      (:server options) (server)
      (:client options) (client))))

