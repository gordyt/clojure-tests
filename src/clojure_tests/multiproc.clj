(ns clojure-tests.multiproc
  (:require [clojure.string :as string]
            [zeromq.zmq :as zmq]
            [clojure.tools.reader.edn :as edn]
            [clojure.tools.cli :refer [parse-opts]])
  (:gen-class))


(def socket-config
  "All the configuration required for switchboard and client configuration"
  {:switchboard {:tcp {:rep "tcp://*:5555"
                       :pub "tcp://*:5556"}
                 :inproc {:rep "inproc://nodereqrep"
                          :pub "inproc://nodepubsub"}}
   :client {:tcp {:rep "tcp://127.0.0.1:5555"
                  :pub "tcp://127.0.0.1:5556"}
            :inproc {:rep "inproc://nodereqrep"
                     :pub "inproc://nodepubsub"}}})
(def context
  "The context to be used for all socket initialization in this process"
  (zmq/context))

(def ^:dynamic 

(defn start-switchboard [config]
  "Configure and start a switchboard, using the specified CONFIG.
See the socket-config defined in this module for an example."




(defn server-wait [clients]
  (printf "server-wait: waiting for clients to report in\n")
  (flush)
  (with-open [socket (doto (zmq/socket context :rep)
                       (zmq/bind inproc-endpoint-advertize)
                       (zmq/bind tcp-endpoint-advertize))]
    (let [client-set (set clients)]
      (loop [known-clients #{}]
        (let* [client-id (zmq/receive-str socket)
               client-ids (conj known-clients client-id)]
              (printf "server-wait: informed of %s\n" client-id)
              (flush)
              (zmq/send-str socket "ACK")
              (when-not (= client-ids client-set)
                      (recur client-ids)))))))
(defn server [clients]
  (printf "server: thread-id=%s\n" (.. Thread currentThread getId))
  (flush)
  (with-open [publisher (doto (zmq/socket context :pub)
                          (zmq/bind inproc-endpoint-publish)
                          (zmq/bind tcp-endpoint-publish))]
    (server-wait clients)

    (doseq [client-id (cycle clients)]
      (let* [val (rand-int 100000)
             msg (pr-str {:name "server" :value val})]
            (printf "server: sending '%s' to '%s' \n" msg client-id)
            (flush)
            (zmq/send-str publisher client-id zmq/send-more)
            (zmq/send-str publisher msg)
            (Thread/sleep 5000)))))

(defn proc-message [id socket]
  (zmq/receive-str socket)  ; read/discard the client-id
  (let [value (-> (zmq/receive-str socket)
                  edn/read-string
                  :value)]
    (printf "proc-message: id=%s received %s\n" id value)
    (flush)))


(defn client-advertise [id]
  (printf "client-advertise: informing about %s\n" id)
  (flush)
  (with-open [socket (doto (zmq/socket context :req)
                       (zmq/connect inproc-endpoint-advertize)
                       (zmq/connect tcp-endpoint-advertize-client))]
    (zmq/send-str socket id)
    (printf "client-advertise: id=%s, reply=%s\n" id (zmq/receive-str socket))
    (flush)))


(defn client [id]
  (printf "client: id=%s, thread-id=%s\n" id (.. Thread currentThread getId))
  (flush)
  (client-advertise id)
  (with-open [subscriber (doto (zmq/socket context :sub)
                           (zmq/connect tcp-endpoint-publish-client)
                           (zmq/connect inproc-endpoint-publish)
                           (zmq/subscribe id))]

    (flush)
    (loop []
      (proc-message id subscriber)
      (recur))))


(def cli-options
  [["-h" "--help" "Print help" :default false]
   ["-s" "--server" "Run the server" :default false]
   ["-l" "--local-clients" "Run the local clients" :default false]
   ["-r" "--remote-clients" "Run the remote clients" :default false]])

  (defn- usage [summary]
  (string/join
   \newline
   ["multiproc - test combination inproc/tcp client/server publish/subscribe."
    ""
    "Usage: program-name [options]"
    summary]))

(defn exit [status msg]
  (println msg)
  (System/exit status))


(defn -main [& args]
  (println "main - initializing: client-ids:" args)
  (flush)
  (let [{:keys [options arguments errors summary]} (parse-opts args cli-options)]
    (when (:help options)
      (exit 0 (usage summary)))
    (when (:server options)
      (future (server arguments))
      ;; inproc must be bound before subscribers can connect
      (Thread/sleep 10))
    (when (:local-clients options)
      (doseq [client-id (filter #(.startsWith % "l") arguments)]
        (printf "Starting local client %s\n" client-id)
        (flush)
        (future (client client-id))))
    (when (:remote-clients options)
      (doseq [client-id (filter #(.startsWith % "r") arguments)]
        (printf "Starting local client %s\n" client-id)
        (flush)
        (future (client client-id))))))





  
  
