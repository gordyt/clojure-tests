(ns clojure-tests.ring-test
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.adapter.jetty :refer [run-jetty]]
            [ring.util.request :refer :all]
            [clojure.tools.reader.edn :as edn]
            [clojure.string :as string]
            [clj-http.client :as http]
            [clojure.tools.cli :refer [parse-opts]])
  (:gen-class))

;; (import java.net.ConnectException)

(def port 1234)
(defonce nodes (ref {}))
            

(def cli-options
  [["-h" "--help" "Print help"]
   ["-s" "--server" "Run the ring server"]
   ["-c" "--client" "Run the test client"]])

(defn- usage [summary]
  (string/join
   \newline
   ["echo-server - echo client/server test program."
    ""
    "Usage: program-name [options]"
    summary]))


(defn client-put [data]
  "Does a PUT to the server with DATA encoded in the body as application/edn.
Returns a map with, at a minimum :status, :content-type, and :body."
  (try
    (let [url (format "http://localhost:%d/advertise" port)]
      (http/put url {:content-type "application/edn"
                     :accept "application/edn"
                     :body (pr-str data)
                     :as :clojure}))
    (catch java.net.ConnectException e
      (printf "client-put exception %s\n" (.getMessage e))
      {:status 503 :content-type "application/edn" :body data})))
  

(def clients [{:n0 {:name "node0" :address "1234"}}
              {:n1 {:name "node1" :address "2345"}}
              {:n2 {:name "node2" :address "3456"}}])

(defn client []
  (println "client test")
  (flush)
  (dorun
   (for [data clients]
     (do
       (printf "Sending: %s\n" data)
       (flush)
       (printf "Received: %s\n" (client-put data))
       (flush)))))


(defn advertise-handler [req]
  (let [ct (content-type req)]
    (if (= ct "application/edn")
      (dosync
       (let [input (edn/read-string (body-string req))
             output (alter nodes merge input)]
         {:status 200
          :headers {"Content-Type" "application/edn"}
          :body (pr-str output)}))
       {:status 400
        :headers {"Content-Type" "text/plain"}
        :body "Must use content-type application/edn"})))
  

(defroutes server-routes
  (GET "/" [] "Nothing to see here")
  (PUT "/advertise" [] (fn [req] (advertise-handler req))))
   
(def server-app
  (->
   server-routes))

(defn server []
  (printf "Starting ring server on port %d\n" port)
  (flush)
  (run-jetty #'server-app {:host "0.0.0.0" :port port :join? false}))


(defn exit [status msg]
  (println msg)
  (System/exit status))

(defn -main [& args]
  (let [{:keys [options arguments errors summary]} (parse-opts args cli-options)]
    (cond
      (:help options) (exit 0 (usage summary))
      (:server options) (server)
      (:client options) (client))))

