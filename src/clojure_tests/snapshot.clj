(ns clojure-tests.snapshot
  (:require [clojure.string :as string]
            [clojure.tools.reader.edn :as edn]
            [clojure.java [io :refer :all]])
  (:gen-class))

(import '[java.nio ByteBuffer]
        '[java.io File])

(def snapshot-file-version 1)
(def buffer-size 4096)

(defn encode-snaphot-bundle
  "Make a snapshot file bundle.  METADATA is a map of arbitrary metadata associated with the
shapshot file.  IN-FILE-PATH is the path to the snapshot file.  OUT-FILE-PATH is the
path to the file this function will create with the following format (not there are
no line-breaks.  This is a binary file:
1
M
<edn-encoded-metadata-lenth-as-long>
<edn-encoded-metadata>
S
<snapshot-file-length-as-long>
<snapshot-file-data>

Returns the size (in bytes) of the snapshot file bundle that was created.
  "
  [metadata in-file-path out-file-path]
  (let [in-file (File. in-file-path)
        in-file-len (.length in-file)
        md-str (pr-str metadata)
        md-str-len (count md-str)
        buf (byte-array buffer-size)
        bb (ByteBuffer/wrap buf)]
    (with-open [out (output-stream out-file-path)
                in (input-stream in-file)]
      (.put bb (byte snapshot-file-version))
      (.putChar bb \M)
      (.putLong bb (long md-str-len))
      (.write out buf 0 (.position bb))
      (.flip bb)
      (.write out (.getBytes md-str))
      (.putChar bb \S)
      (.putLong bb (long in-file-len))
      (.write out buf 0 (.position bb))

      (loop [bytes-read (.read in buf 0 buffer-size)]
        (when (pos? bytes-read)
          (.write out buf 0 bytes-read)
          (recur (.read in buf 0 buffer-size)))))
    (.length (File. out-file-path))))


(defn decode-snapshot-bundle
  "Parse the snapshot file bundle created by encode-shapshot-bundle and that is pointed
to by IN-FILE-PATH.  Returns the metadata map and reconstructs the snapshot file
at OUT-FILE-PATH."
  [in-file-path out-file-path]
  (with-open [in (input-stream in-file-path)
              out (output-stream out-file-path)]
    (let [buf (byte-array buffer-size)
          bb (ByteBuffer/wrap buf)]
      (.read in buf 0 11)
      (let [version (.get bb)
            md-marker (.getChar bb)
            md-size (.getLong bb)
            md-buf (if (< md-size buffer-size) buf (byte-array md-size))
            md-bytes (.read in md-buf 0 md-size)
            metadata (edn/read-string (string/join (map char (take md-size md-buf))))]
        (.flip bb)
        (.read in buf 0 10)
        (let [ss-marker (.getChar bb)
              ss-size (.getLong bb)]
          (loop [bytes-read (.read in buf 0 buffer-size)]
            (when (pos? bytes-read)
              (.write out buf 0 bytes-read)
              (recur (.read in buf 0 buffer-size)))))
        metadata))))




(require '(clojure.java [jdbc :as j]))

;;
(def log-db {:subprotocol "sqlite"
             :subname "/Users/gordy/tmp/log.dat"})

(def state-db {:subprotocol "sqlite"
               :subname "/Users/gordy/tmp/state.dat"})



(j/query state-db
         ["select log_idx from state where machine = ?" "patch"])

(j/execute! state-db ["backup database



(j/db-do-commands sqlite-db (j/create-table-ddl :events
                          [:id "integer" :primary :key]
                          [:term "integer" :not :null]
                          [:data "text"]
                          :table-spec "without rowid"))


(j/create-table-ddl :events
                          [:id "integer" :primary :key]
                          [:term "integer" :not :null]
                          [:data "text"]
                          :table-spec "without rowid")

"CREATE TABLE events (id integer primary key, term integer not null, data text) without rowid"

(j/insert! sqlite-db :events {:id 1 :term 1 :data "my event"})
