(ns clojure-tests.actor-macro-test
  (:require [clojure.test :refer :all]
            [clojure-tests.config :refer :all]
            [clojure.tools.reader.edn :as edn]
            [clojure.reflect :refer [reflect type-reflect]]
            [clojure.core.async :refer [chan <!! >!! close!]]))


(defprotocol Actor
  "Implement the Actor protocol when you need to be able to message an
  independent thread, synchronously or asynchronously."
  (thread-cb [this])
  (start [this] "Start the actor.  TODO - we may not need this.")
  (stop [this] "Stop the actor."))


(defprotocol MathMunger
  ;; NOTE: You cannot use rest args in protocol; e.g. (add [this & nums])
  (add [this nums])
  (divide [this numerator denominator]))

(defprotocol Counter
  (reset-counter [this value])
  (increment-counter [this amount])
  (get-counter [this]))

(deftype MathMungerLocal [^:volatile-mutable counter]
  Counter
  (reset-counter [this value]
    (set! counter value))
  (increment-counter [this amount]
    (set! counter (+ counter amount)))

  (get-counter [this]
    counter)
  MathMunger
  (add [this nums]
    (apply + nums))
  (divide [this numerator denominator]
    (/ numerator denominator)))



(defprotocol Simple
  (adder [this x] [this x y]))

(deftype SimpleLocal []
  Simple
  (adder [this x] (+ x x))
  (adder [this x y] (+ x y)))


(defmacro actor-wrapper [name protocols]
  (let [channel (gensym 'channel)
        this# (gensym 'this)
        argbodies# (fn [fname# alists#]
                     (map (fn [al#]
                            (let [al2# (cons this# (rest al#))]
                              (list
                               fname#
                               (vec al2#)
                               `(let [c# (chan)]
                                  (>!! ~channel (list
                                                (fn [~this#]
                                                  ~(cons fname# al2#))
                                                c#))
                                  (<!! c#)))))
                          alists#))
        functions# (mapcat (fn [p#]
                             (let [pp# (if (symbol? p#) (eval p#) p#)]
                               (cons p#
                                     (mapcat #(argbodies# (:name %) (:arglists %))
                                          (vals (:sigs pp#))))))
                           protocols)]
    `(deftype ~name [~channel wrapped#]
       Actor
       (thread-cb [this#]
         (loop [[cmd# rchan#] (<!! ~channel)]
           (when cmd#
             (try
               (>!! rchan# (cmd# wrapped#))
               (catch Exception e# (do (printf "exception: %s\n" (.getMessage e#)) (flush)))
               (finally (close! rchan#)))
             (recur (<!! ~channel)))))
       (start [this#]
         (future (thread-cb this#)))
       (stop [this#]
         (>!! ~channel [nil nil]))
       ~@functions#)))


;; (def sl (->SimpleLocal))

;; (actor-wrapper SimpleActor [Simple])

;; (def sa (->SimpleActor (chan) sl))


;; (start sa)

;; (adder sa 1)

;; (adder sa 1 2)

;; (stop sa)
