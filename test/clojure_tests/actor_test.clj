(ns clojure-tests.actor-test
  (:require [clojure.test :refer :all]
            [clojure-tests.config :refer :all]
            [clojure.tools.reader.edn :as edn]
            [clojure.core.async :refer [chan <!! >!! close!]]))


(defprotocol Actor
  "Implement the Actor protocol when you need to be able to message an
  independent thread, synchronously or asynchronously."
  (thread-cb [this])
  (start [this] "Start the actor.  TODO - we may not need this.")
  (stop [this] "Stop the actor."))


(defprotocol MathMunger
  ;; NOTE: You cannot use rest args in protocol; e.g. (add [this & nums])
  (add [this nums])
  (divide [this numerator denominator]))


(deftype MathMungerLocal []
  MathMunger
  (add [this nums]
    (apply + nums))
  (divide [this numerator denominator]
    (/ numerator denominator)))


(deftype MathMungerAgent [channel]
  Actor
  (thread-cb [this]
    (let [local (MathMungerAgent. nil)]
      (loop [[rchan cmd] (<!! channel)]
        (when cmd
          (if rchan
            (try
              (>!! rchan (cmd local))
              (catch Exception e (do
                                   (printf "exception: %s\n" (.getMessage e))
                                   (flush)))
              (finally (close! rchan))))
          (cmd local))
        (recur (<!! channel)))))
  (start [this]
    (future (thread-cb this)))
  (stop [this]
    (>!! channel (list nil (pr-str nil))))
  MathMunger
  (add [this nums]
    (if channel
      (let [c (chan)]
        (>!! channel (list c (fn [lc] (add lc nums))))
        (<!! c))
      (apply + nums)))
  (divide [this numerator denominator]
    (if channel
      (let [c (chan)]
        (>!! channel (list c (fn [lc] (divide lc numerator denominator))))
        (<!! c)))
    (/ numerator denominator)))



(defn mathmungeragent [channel]
  (->MathMungerAgent channel))


(deftest math-munger-local-test
  (testing "basic MathMunger with no threading"
    (let [mml (->MathMungerLocal)]
      (is (= 10 (add mml [1 2 3 4])))
      (is (= 1/2 (divide mml 1 2))))))

(deftest math-munger-agent-test
  (testing "basic MathMunger with threading"
    (let [mml (->MathMungerAgent (chan))]
      (start mml)
      (is (= 10 (add mml [1 2 3 4])))
      (is (= 1/2 (divide mml 1 2)))
      (stop mml))))
