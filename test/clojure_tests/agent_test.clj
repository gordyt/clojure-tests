(ns clojure-tests.agent-test
  (:require [clojure.test :refer :all]
            [clojure-tests.config :refer :all]))

(defn thread-id []
  (.getId (Thread/currentThread)))

(defn log [msg]
  (printf "***** Thread %s *****\n" (thread-id))
  (println msg)
  (flush))

(def cluster (agent {}))

(add-watch cluster :cluster (fn [k r os ns]
                              (log
                               (format ":cluster os=%s, ns=%s" os ns))))


(deftest cluster-agent-test
  (testing "cluster agent test"
    (log "starting cluster-agent-test")
    (send cluster assoc :n0 {:name "node0" :address "1234"})
    (send cluster assoc :n1 {:name "node1" :address "2345"})
    (send cluster assoc :n2 {:name "node2" :address "3456"})
    (Thread/sleep 1000)
    (is (= 0 0))))
