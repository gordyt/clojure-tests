(ns clojure-tests.bar)
(defmulti create-shard
  (fn [cfg] (:shard-type cfg)))

(defmethod create-shard :default [cfg]
  "default-shard")
