(ns clojure-tests.better-actor
  (:require [clojure.test :refer :all]
            [clojure-tests.config :refer :all]
            [clojure.tools.reader.edn :as edn]
            [clojure.core.async :refer [chan <!! >!! close!]]))


(defn actor-message-loop
  "This function will spin up a message processing loop in a thread.
  It returns a channel that can be used to send commands to the
  message-loop thread.  The commands that are sent on the channel
  should look like this: [cmd c], where:
  cmd is an fn or function that takes one argument (that is the actor-state
  that was passed into his function, initially) and c is a channel used
  to sent the response back on.   This channel c will be closed after the
  reponse is sent.  The cmd fn must return the following:
  [response new-state].  The response is what is sent back on the response
  channel and new-state will be the state value passed in as an argument to the
  next function that is sent."
  [actor-state]
  (let [input-channel (chan)
        loopfn (fn [channel state]
                 (loop [[cmd rchan] (<!! channel)
                        state state]
                   (if cmd
                     (let [[resp newstate] (cmd state)]
                       (>!! rchan resp)
                       (close! rchan)
                       (recur (<!! channel) newstate))
                     (close! channel))))]
    (future (loopfn input-channel actor-state))
    input-channel))


(defprotocol MessageLoop
  "Simple protocol for working with a channel that is returned
  from actor-message-loop."
  (send-cmd [this cmd]
    "Send a command CMD to the message loop.  Commands MUST
     be a function that takes one argument (state) and returns
     [response new-state].")
  (send-stop [this]
    "Terminate the thread that is started by actor-message-loop."))


(deftype ActorLoop [channel]
  MessageLoop
  (send-cmd [this cmd]
    (let [c (chan)]
      (>!! channel [cmd c])
      (<!! c)))
  (send-stop [this]
    (>!! channel [nil nil])))

(defn make-message-loop
  "Returns and instance of a MessageLoop that is ready to process commands."
  [initial-state]
  (->ActorLoop (actor-message-loop initial-state)))


(deftest message-loop-test
  (testing "the funcionality of the new message loop"
    (let [ml (make-message-loop {})
          getstate (fn [s] (list s s))
          setstate (fn [key value] (fn [s]
                                     (let [ns (assoc s key value)] [ns ns])))]
      (is (= (send-cmd ml getstate) {}))
      (is (= (send-cmd ml (setstate :name "gordon")) {:name "gordon"}))
      (is (= (send-cmd ml getstate) {:name "gordon"}))
      (send-stop ml))))
