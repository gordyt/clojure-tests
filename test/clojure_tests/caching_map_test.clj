(ns clojure-tests.caching-map-test
  (:require [clojure.test :refer :all]
            [clojure-tests.config :refer :all]
            [clojure.tools.reader.edn :as edn]))



(deftype CachingMap [cache]
  clojure.lang.ILookup
  (valAt [this k]
    (@cache k))
  (valAt [this k d]
    (@cache k d))
  clojure.lang.IPersistentMap
  (assoc [this k v]
    (dosync (alter cache assoc k v))
    this)
  (without [this k]
    (dosync (alter cache dissoc k))
    this)
  clojure.lang.IPersistentCollection
  (equiv [this other]
    (= @cache other))
  clojure.lang.Seqable
  (seq [_]
    (seq @cache)))

(def *foo* (->CachingMap (ref {})))

(assoc *foo* :name "gordon")
(assoc *foo* :city "Houston")
;; {:name "gordon"}
(seq *foo*)
;; ([:name "gordon"])
(dissoc *foo* :name)
(seq *foo*)

*foo*

(= *foo* {})

(def *bar* {:name "gordon" :city "Houston"})
*bar*
(seq *bar*)
(= *foo* *bar*)
