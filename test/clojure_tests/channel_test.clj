(ns clojure-tests.channel-test
  (:require [clojure.test :refer :all]
            [clojure-tests.config :refer :all]
            [clojure.core.async :refer [chan <!! >!! close!]]))

(defn thread-id []
  (.getId (Thread/currentThread)))

(defn log [msg]
  (printf "***** Thread %s *****\n" (thread-id))
  (println msg)
  (flush))


(defn receive [channel]
  (loop [msg (<!! channel)]
    (condp = msg
      "quit" (log "Received quit message")
      (let [[rchan task] msg]
        (log (format "Received rchan %s (%s), task %s" rchan (type rchan) task))
        (>!! rchan (format "echo: %s" task))
        (close! rchan)
        (recur (<!! channel))))))

(deftest receive-test
  (testing "simulating an erlang receive loop"
    (let [schan (chan)
          shard (future (receive schan))
          rchan (chan)]
      (log "sending 'hello' to worker")
      (>!! schan (list rchan "hello"))
      (log "waiting for return message from worker")
      (is (= (<!! rchan) "echo: hello"))
      (>!! schan "quit")
      (is (nil? @shard)))))


(defprotocol ShardProtocol
  (stop [this])
  (put [this id val])
  (get [this id])
  (get-config [this])
  (set-config [this config]))


(deftype ShardType [config, channel]
  ShardProtocol
  (stop [_] (do (>!! channel (list :quit nil nil)) nil))
  (put [_ id val]
    (let [c (chan)]
      (>!! channel (list :put c [id val]))
      (<!! c)))
  (get [_ id]
    (let [c (chan)]
      (>!! channel (list :get c [id]))
      (<!! c)))
  (get-config [_]
    (let [c (chan)]
      (>!! channel (list :get-config c nil))
      (<!! c)))
  (set-config [_ config]
    (let [c (chan)]
      (>!! channel (list :set-config c [config]))
      (<!! c))))


(defn shard-worker [config channel]
  (loop [config config
         state {}
         [cmd c args] (<!! channel)]
    (condp = cmd
      :quit nil
      :put (do (>!! c true)
               (close! c)
               (recur config (assoc state (first args) (second args)) (<!! channel)))
      :get (do (>!! c (state (first args)))
               (close! c)
               (recur config state (<!! channel)))
      :get-config (do (>!! c config)
                      (close! c)
                      (recur config state (<!! channel)))
      :set-config (do (>!! c true)
                      (close! c)
                      (recur (first args) state (<!! channel))))))

(defn shard [config]
  ;; setup whatever closures we need
  ;; then run receive loop
  (let [channel (chan)
        thread (future (shard-worker config channel))]
    (ShardType. config channel)))


(deftest shard-test
  (testing "simulating a better shard"
    (let [s (shard {})]
      (is (put s "name" "gordon"))
      (is (put s "age" 32))
      (is (= (get s "name") "gordon"))
      (is (= (get s "age") 32))
      (is (= (get-config s) {}))
      (is (set-config s {:nodes #{:n0 :n1 :n2}}))
      (is (= (get-config s) {:nodes #{:n0 :n1 :n2}}))
      (is (nil? (stop s))))))
