(ns clojure-tests.channel-thread-test
  (:require [clojure.test :refer :all]
            [clojure-tests.config :refer :all]
            [clojure.core.async :refer [chan <!! >!! close!]]))

(defn thread-id []
  (.getId (Thread/currentThread)))

(defn log [msg]
  (printf "***** Thread %s *****\n" (thread-id))
  (println msg)
  (flush))


(defprotocol ShardProtocol
  (do-cmd [this cmd args])
  (stop [this])
  (put-state [this id val])
  (get-state [this id])
  (get-config [this])
  (set-config [this config]))


(deftype TestShard [channel]
  ShardProtocol
  (do-cmd [this cmd args]
    (let [c (chan)]
      (>!! channel (list cmd c args))
      (<!! c)))
  (stop [_] (do (>!! channel (list :quit nil nil)) nil))
  (put-state [this id val]
    (do-cmd this :put [id val]))
  (get-state [this id]
    (do-cmd this :get [id]))
  (get-config [this]
    (do-cmd this :get-config nil))
  (set-config [this config]
    (do-cmd this :set-config [config])))


(defn shard-worker [channel config]
  (loop [config config
         state {}
         [cmd rchan & args] (<!! channel)]
    (printf "shard-worder: config=%s, state=%s, cmd=%s args=%s\n"
            config state cmd args) (flush)
    (let [[resp nconfig nstate] (condp = cmd
                                  :quit [nil nil nil]
                                  :put [true config (assoc state (ffirst args) (first (nfirst args)))]
                                  :get [(state (ffirst args)) config state]
                                  :get-config [config config state]
                                  :set-config [true (ffirst args) state]
                                  [nil config state])]
      (when (and nconfig nstate)
        (>!! rchan resp)
        (close! rchan)
        (recur nconfig nstate (<!! channel))))))

(defn shard [config]
  ;; setup whatever closures we need
  ;; then run receive loop
  (let [channel (chan)
        thread (future (shard-worker channel config))]
    (TestShard. channel)))


(deftest shard-test
  (testing "simulating a better shard"
    (let [s (shard {})]
      (is (put-state s "name" "gordon"))
      (is (put-state s "age" 32))
      (is (= (get-state s "name") "gordon"))
      (is (= (get-state s "age") 32))
      (is (= (get-config s) {}))
      (is (set-config s {:nodes #{:n0 :n1 :n2}}))
      (is (= (get-config s) {:nodes #{:n0 :n1 :n2}}))
      (is (nil? (stop s))))))


(defprotocol SampleProto
  (start [this])
  (stop [this])
  (thread-cb [this])
  (log-msg [this msg]))


(deftype SampleTest [channel]
  SampleProto
  (thread-cb [this]
    (loop [[cmd rchan & args] (<!! channel)]
      (printf "thread-cb: cmd=%s, args=%s\n" cmd args) (flush)
      (let [resp (condp = cmd
                   :log-msg (printf "log-msg: %s\n" (first args))
                   :quit nil)]
        (when rchan
          (>!! rchan resp)
          (close! rchan))
        (when-not (= cmd :quit)
                (recur (<!! channel))))))
  (start [this]
    (future (thread-cb this)))
  (stop [this]
    (>!! channel (list :quit nil nil)))
  (log-msg [this msg]
    (>!! channel (list :log-msg nil msg))))



(ns-unmap *ns* '->SampleTest)
(defn ->SampleTest []
  (printf "->SampleTest Constructor\n") (flush)
  (SampleTest. (chan)))


(deftest type-managed-thread-test
  (testing "have the type that implements the protocol manage the thread"
    (let [st (->SampleTest)]
      (start st)
      (log-msg st "This is message one")
      (log-msg st "This is message two")
      (stop st)
      (is true))))
