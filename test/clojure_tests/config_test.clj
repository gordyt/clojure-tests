(ns clojure-tests.config-test
  (:require [clojure.test :refer :all]
            [clojure-tests.config :refer :all]
            [clojure.tools.reader.edn :as edn]))

(deftest load-config-1-test
  (testing "load one config file"
    (let [cfg (load-config "test/clojure_tests/load-config-1-test.edn")]
      (is (= (:hostname cfg) "localhost"))
      (is (= (:host (:database cfg)) "my.host"))
      (is (= (:port (:database cfg)) 1234))
      (is (= (:name (:database cfg)) "my-name"))
      (is (= (:user (:database cfg)) "my-user")))))

(deftest load-config-2-test
  (testing "loading/merging 2 config files"
    (let [cfg (load-config "test/clojure_tests/load-config-1-test.edn"
                           "test/clojure_tests/load-config-2-test.edn")]
      (is (= (:hostname cfg) "localhost"))
      (is (= (:host (:database cfg)) "your.host"))
      (is (= (:port (:database cfg)) 4567))
      (is (= (:name (:database cfg)) "my-name"))
      (is (= (:user (:database cfg)) "my-user")))))

(deftest load-config-edns-test
  (testing "loading configuration from EDN strings"
    (let [cfg (load-config-edns
               "{:hostname \"localhost\"
                :database {:host \"my.host\"
                           :port 1234
                           :name \"my-name\"
                           :user \"my-user\"}}"
               "{:database {:host \"your.host\"
                           :port 4567}}")]
      (is (= (:hostname cfg) "localhost"))
      (is (= (:host (:database cfg)) "your.host"))
      (is (= (:port (:database cfg)) 4567))
      (is (= (:name (:database cfg)) "my-name"))
      (is (= (:user (:database cfg)) "my-user")))))

(deftest load-config-maps-test
  (testing "loading configuration from maps"
    (let [cfg (load-config-maps
               {:hostname "localhost"
                :database {:host "my.host"
                           :port 1234
                           :name "my-name"
                           :user "my-user"}}
               {:database {:host "your.host"
                           :port 4567}})]
      (is (= (:hostname cfg) "localhost"))
      (is (= (:host (:database cfg)) "your.host"))
      (is (= (:port (:database cfg)) 4567))
      (is (= (:name (:database cfg)) "my-name"))
      (is (= (:user (:database cfg)) "my-user")))))
