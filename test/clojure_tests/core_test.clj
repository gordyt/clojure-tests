(ns clojure-tests.core-test
  (:require [clojure.test :refer :all]
            [clojure-tests.core :refer :all]))

(deftest a-test
  (testing "a-test - trivial nothing test"
    (is (= 1 1))))
