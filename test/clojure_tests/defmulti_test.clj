(ns clojure-tests.defmulti-test
  (:require [clojure.test :refer :all]
            [clojure-tests.core :refer :all]
            [clojure-tests.bar :as bar]))

;;; This is one way to make the defmethods available
            ;; [clojure-tests.baz]
            ;; [clojure-tests.bin]))

;;; This is another way
;; (require 'clojure-tests.baz)
;; (require 'clojure-tests.bin)

;;; And so is this
;; (require (symbol "clojure-tests.baz"))
;; (require (symbol "clojure-tests.bin"))


;;; Which can be encoded in a list of strings, as if we were processing a configuration
;;; value!
(doseq [n ["clojure-tests.baz" "clojure-tests.bin"]]
  (require (symbol n)))


(deftest bar-bas-bin-test
  (testing "bar-bas-bin"
    (let [x (bar/create-shard {:shard-type :dir-shard})
          y (bar/create-shard {:shard-type :sqlite-shard})
          z (bar/create-shard {:shard-type :unknown})]
      (is (= x "dir-shard"))
      (is (= y "sqlite-shard"))
      (is (= z "default-shard")))))
