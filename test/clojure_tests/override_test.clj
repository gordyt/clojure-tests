(ns clojure-tests.override-test
  (:require [clojure.test :refer :all]
            [clojure-tests.config :refer :all]))


(defprotocol Greeter
  (get-name [_])
  (greet [_]))

(defrecord BasicGreeter [name])
(defrecord FriendlyGreeter [greeter greeting])
(defrecord AdvancedGreeter [greeter greeting])

(def base-greeter {:get-name (fn [g] (:name g))
                   :greet (fn [_] "yo")})

(extend BasicGreeter
  Greeter
  base-greeter)

(extend FriendlyGreeter
  Greeter
  (merge base-greeter {:greet (fn [g] (format "%s, my name is %s"
                                              (:greeting g)
                                              (get-name g)))
                       :get-name (fn [g] (get-name (:greeter g)))}))

(defn- wrap-methods
  "Automatically make all base-class methods use the proper value that is
  embedded in the sub-class.  METHOD-MAP is a map of the base-class methods
  and BASE-KEY is the key in the sub-class that contains the base-class instance."
  [method-map base-key]
  (apply merge (map
                (fn [[k f]] {k (fn [g] (f (base-key g)))})
                method-map)))


;;; AdvancedGreeter is advanced because any functions that it
;;; doesn't explicitly override will automatically used the properly-
;;; dereferenced base class method.
(extend AdvancedGreeter
  Greeter
  (merge (wrap-methods base-greeter :greeter) ; properly-wrapped base class methods
         ;; methods we are overriding in this sub-class
         {:greet (fn [g] (format "%s, my name is %s"
                                 (:greeting g)
                                 (get-name g)))}))


(deftest greeter-test
  (testing "basic and friendly greeters"
    (let [bg (->BasicGreeter "bob")
          fg (->FriendlyGreeter (->BasicGreeter "sue") "Hello")
          ag (->AdvancedGreeter (->BasicGreeter "sam") "Greetings")]
      (is (= (get-name bg) "bob"))
      (is (= (get-name fg) "sue"))
      (is (= (get-name ag) "sam"))
      (is (= (greet bg) "yo"))
      (is (= (greet fg) "Hello, my name is sue"))
      (is (= (greet ag) "Greetings, my name is sam")))))
