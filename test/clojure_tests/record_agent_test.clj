(ns clojure-tests.record-agent-test
  (:require [clojure.test :refer :all]
            [clojure-tests.config :refer :all]
            [clojure.tools.reader.edn :as edn]
            [clojure.reflect :refer [reflect type-reflect]]
            [clojure.core.async :refer [chan <!! >!! close!]]))

;;;; Here I am just testing to see if agents created in a let block or a deftype
;;;; args can be updated by internal code.


(defprotocol Statefull
  (set-state [this key value])
  (get-state [this key] [this key default])
  (incr-state [this key amount]))

(deftype StatefullType [state-agent]
  Statefull
  (set-state [this key value]
    (send state-agent assoc-in [key] value))
  (get-state [this key]
    (get-state this key nil))
  (get-state [this key default]
    (await state-agent)
    (@state-agent key default))
  (incr-state [this key amount]
    (send state-agent update-in [key] #(+ (or %1 0) amount))))


(defn make-stateful
  ([] (make-stateful {}))
  ([initial-map] (->StatefullType (agent initial-map))))


(deftest statefull-1
  (testing "updating an agent wrapped in a type"
    (let [a (make-stateful)]
      (set-state a :name "gordon")
      (set-state a :age 32)
      (is (= (get-state a :name) "gordon"))
      (is (= (get-state a :age) 32))
      (incr-state a :age 1)
      (is (= (get-state a :age) 33)))))
