(ns clojure-tests.ring-test
  (:require [clojure.test :refer :all]
            [clojure-tests.config :refer :all]
            [clj-http.client :as http]
            [ring.adapter.jetty :refer [run-jetty]]))




(defn test-handler
  [req]
  {:status 200
   :headers {"Content-Type" "text/plain"}
   :body "OK"})


(defn test-fixture [f]
  ;; setup
  (let [jetty-instance (run-jetty test-handler
                                  {:port 2283 :join? false})]
    (f)
    ;; teardown
    (.stop jetty-instance)
    ))

(use-fixtures :once test-fixture)

(defn message- []
  (let  [url "http://localhost:2283"
         t0 (.getTime (java.util.Date.))
         msg (:body (http/get url))
         t1 (.getTime (java.util.Date.))]
    (- t1 t0)))


(deftest ring-server-test
  (testing "verify server response"
    (let [msg (:body (http/get "http://localhost:2283"))]
      (is (= msg "OK")))))

(deftest ring-message-test
  (testing "verify our little message- function"
    (let [t (message-)]
      (is (integer? t))
      (is (pos? t)))))


(deftest ring-speed-test-single
  (testing "Round-trip time to make a request and get back a response"
    (let [cnt 1000
          sum (apply + (repeatedly cnt message-))
          avg (double (/ sum cnt))]
      (println "ring-speed-test-single") (flush)
      (printf "%s calls took an average of %s milliseconds each\n" cnt avg)
      (flush))))


(deftest ring-speed-test-multiple
  (testing "Round-trip averages with multiple client threads"
    (let [cnt 1000
          nclients 10
          cfunc (fn [_] (let [sum (apply + (repeatedly cnt message-))
                             avg (double (/ sum cnt))]
                         avg))
          threads (map #(future (cfunc %1)) (range nclients))
          values (map deref threads)
          sums (apply + values)
          avg-avg (double (/ sums nclients))
          min-avg (apply min values)
          max-avg (apply max values)]
      (println "ring-speed-test-multiple") (flush)
      (printf "with %s clients, each making %s calls:\n" nclients cnt) (flush)
      (printf "min-avg=%s ms, max-avg=%s ms, avg-avg=%s ms\n" min-avg max-avg avg-avg) (flush))))
