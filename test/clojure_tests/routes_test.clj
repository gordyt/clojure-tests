(ns clojure-tests.routes-test
  (:require [clojure.test :refer :all]
            [clojure-tests.config :refer :all]
            [clojure.tools.reader.edn :as edn]
            [clj-http.client :as http]
            [compojure.core :refer :all]
            [ring.adapter.jetty :refer [run-jetty]]
            [ring.util.request :refer [content-type]]))

(defn- index
  "Handle GET request on /.  CONTEXT is an arbitrary
  map that we can use for whatever."
  [context request]
  {:status 200
   :headers {"Content-Type" "application/edn"}
   :body (pr-str context)})

(defn- test-routes [context]
  (routes
   (GET "/" [] (partial index context))))

(deftest basic-routes-test
  (testing "basic composure routes"
    (let [c {:name "gordon" :city "houston"}
          j (run-jetty (test-routes c)
                       {:port 2283 :join? false})
          resp (http/get "http://localhost:2283")
          ct (content-type resp)
          msg (edn/read-string (:body resp))]
      (is (= ct "application/edn"))
      (is (= msg c))
      (.stop j))))
