(ns clojure-tests.var-test
  (:require [clojure.test :refer :all]
            [clojure-tests.core :refer :all]))

(defn- make-ref [v]
  (ref v))

(defn- make-val []
  (rand-int 1000))

(defn- future-func []
  (let [rv (make-ref(make-val))]
    (printf "future-func: my value is %s\n" @rv)
    @rv))

(deftest thread-local-var-test
  (testing "thread-local-var-test"
    (let [f1 (future (future-func))
          f2 (future (future-func))
          f3 (future (future-func))]
      (is (not= @f1 @f2 @f3)))))

(def a-ref (ref 0))

(defn- future-func2 [n]
  (let [rv (make-val)]
    (printf "future-func %s: my value is %s\n" n rv)
    (dosync (ref-set a-ref rv))
    @a-ref))

(deftest shared-var-test
  (testing "thread-local-var-test"
    (let [f1 (future (future-func2 "f1"))
          f2 (future (future-func2 "f2"))
          f3 (future (future-func2 "f3"))
          a @a-ref]
      (is (not= @f1 a))
      (is (not= @f2 a))
      (is (not= @f3 a)))))


(defn footest []
  (let [f1 (future (future-func2 "f1"))
        f2 (future (future-func2 "f2"))
        f3 (future (future-func2 "f3"))
        a @a-ref]
    [@f1 @f2 @f3 a]))
